import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;
import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import java.awt.Component;
import javax.swing.JSeparator;
import javax.swing.JEditorPane;
import javax.swing.ImageIcon;

public class ventana1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtIntroduzcaElNombre;
	private JTextField txtIntroduzaElNombre;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtIntroduzaElNombre_1;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField txtCantidad;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField txtSuma;
	private JTextField txttieneDescuento;
	private JTextField txtdeQueTipo;
	private JTextField txtProcesandoCuenta;
	private JTextField txtvaAPagar;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField txtImagenDelProducto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana1 frame = new ventana1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana1() {
		setTitle("Ventana 1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1252, 695);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNuevaVentana = new JMenuItem("Nueva ventana");
		mnNewMenu.add(mntmNuevaVentana);
		
		JMenu mnNewMenu_1 = new JMenu("Guardar");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como");
		mnNewMenu_1.add(mntmGuardarComo);
		
		JMenu mnNewMenu_2 = new JMenu("Editar");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnNewMenu_2.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnNewMenu_2.add(mntmPegar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 1230, 22);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Abrir");
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Guardar");
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Cerrar");
		toolBar.add(btnNewButton_2);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 38, 1192, 554);
		contentPane.add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.inactiveCaption);
		tabbedPane.addTab("Productos", null, panel_2, null);
		panel_2.setLayout(null);
		
		txtIntroduzaElNombre_1 = new JTextField();
		txtIntroduzaElNombre_1.setText("Introduza el nombre del producto");
		txtIntroduzaElNombre_1.setOpaque(false);
		txtIntroduzaElNombre_1.setEditable(false);
		txtIntroduzaElNombre_1.setColumns(10);
		txtIntroduzaElNombre_1.setBounds(30, 16, 263, 26);
		panel_2.add(txtIntroduzaElNombre_1);
		
		JButton btnNuevoProfesor = new JButton("Producto");
		btnNuevoProfesor.setBounds(366, 48, 196, 79);
		panel_2.add(btnNuevoProfesor);
		
		JTextPane txtpnDescripcinDelProfesor = new JTextPane();
		txtpnDescripcinDelProfesor.setText("Descripci\u00F3n del producto");
		txtpnDescripcinDelProfesor.setOpaque(false);
		txtpnDescripcinDelProfesor.setEditable(false);
		txtpnDescripcinDelProfesor.setBounds(30, 167, 188, 26);
		panel_2.add(txtpnDescripcinDelProfesor);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(327, 182, 291, 221);
		panel_2.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JTextPane txtpnAosSiendoProfesor = new JTextPane();
		txtpnAosSiendoProfesor.setText("Precio");
		txtpnAosSiendoProfesor.setOpaque(false);
		txtpnAosSiendoProfesor.setEditable(false);
		txtpnAosSiendoProfesor.setBounds(73, 478, 64, 26);
		panel_2.add(txtpnAosSiendoProfesor);
		
		JTextPane txtpnMduloQueImparte = new JTextPane();
		txtpnMduloQueImparte.setText("Tipo de producto");
		txtpnMduloQueImparte.setOpaque(false);
		txtpnMduloQueImparte.setEditable(false);
		txtpnMduloQueImparte.setBounds(30, 436, 148, 26);
		panel_2.add(txtpnMduloQueImparte);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Bebida", "Comida"}));
		comboBox.setBounds(225, 436, 96, 33);
		panel_2.add(comboBox);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(30, 48, 263, 79);
		panel_2.add(textField);
		
		JSlider slider = new JSlider();
		slider.setMajorTickSpacing(1);
		slider.setToolTipText("");
		slider.setValue(5);
		slider.setSnapToTicks(true);
		slider.setPaintLabels(true);
		slider.setMinimum(1);
		slider.setMaximum(10);
		slider.setBounds(181, 474, 188, 46);
		panel_2.add(slider);
		
		txtImagenDelProducto = new JTextField();
		txtImagenDelProducto.setText("Imagen del producto");
		txtImagenDelProducto.setOpaque(false);
		txtImagenDelProducto.setEditable(false);
		txtImagenDelProducto.setColumns(10);
		txtImagenDelProducto.setBounds(793, 144, 163, 26);
		panel_2.add(txtImagenDelProducto);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(ventana1.class.getResource("/images/descarga.jpg")));
		btnNewButton_3.setBounds(723, 182, 263, 183);
		panel_2.add(btnNewButton_3);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.info);
		tabbedPane.addTab("Cuenta", null, panel, null);
		panel.setLayout(null);
		
		txtIntroduzcaElNombre = new JTextField();
		txtIntroduzcaElNombre.setBounds(28, 68, 263, 48);
		panel.add(txtIntroduzcaElNombre);
		txtIntroduzcaElNombre.setColumns(10);
		
		txtIntroduzaElNombre = new JTextField();
		txtIntroduzaElNombre.setEditable(false);
		txtIntroduzaElNombre.setOpaque(false);
		txtIntroduzaElNombre.setText("Introduza el nombre del Producto");
		txtIntroduzaElNombre.setBounds(28, 26, 263, 26);
		panel.add(txtIntroduzaElNombre);
		txtIntroduzaElNombre.setColumns(10);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setBounds(135, 188, 32, 26);
		panel.add(spinner_2);
		
		textField_1 = new JTextField();
		textField_1.setText("Introduza el nombre del Producto");
		textField_1.setOpaque(false);
		textField_1.setEditable(false);
		textField_1.setColumns(10);
		textField_1.setBounds(306, 26, 263, 26);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setText("Introduza el nombre del Producto");
		textField_2.setOpaque(false);
		textField_2.setEditable(false);
		textField_2.setColumns(10);
		textField_2.setBounds(584, 26, 263, 26);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setText("Introduza el nombre del Producto");
		textField_3.setOpaque(false);
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(862, 26, 263, 26);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(306, 68, 263, 48);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(584, 68, 263, 48);
		panel.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(862, 68, 263, 48);
		panel.add(textField_6);
		
		txtCantidad = new JTextField();
		txtCantidad.setText("Cantidad");
		txtCantidad.setOpaque(false);
		txtCantidad.setEditable(false);
		txtCantidad.setColumns(10);
		txtCantidad.setBounds(114, 146, 73, 26);
		panel.add(txtCantidad);
		
		textField_7 = new JTextField();
		textField_7.setText("Cantidad");
		textField_7.setOpaque(false);
		textField_7.setEditable(false);
		textField_7.setColumns(10);
		textField_7.setBounds(399, 146, 73, 26);
		panel.add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setText("Cantidad");
		textField_8.setOpaque(false);
		textField_8.setEditable(false);
		textField_8.setColumns(10);
		textField_8.setBounds(680, 146, 73, 26);
		panel.add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setText("Cantidad");
		textField_9.setOpaque(false);
		textField_9.setEditable(false);
		textField_9.setColumns(10);
		textField_9.setBounds(961, 146, 73, 26);
		panel.add(textField_9);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(421, 188, 32, 26);
		panel.add(spinner_1);
		
		JSpinner spinner_3 = new JSpinner();
		spinner_3.setBounds(703, 188, 32, 26);
		panel.add(spinner_3);
		
		JSpinner spinner_4 = new JSpinner();
		spinner_4.setBounds(983, 188, 32, 26);
		panel.add(spinner_4);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 236, 1187, 2);
		panel.add(separator);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(0, 267, 185, 253);
		panel.add(textPane);
		
		txtSuma = new JTextField();
		txtSuma.setText("Suma");
		txtSuma.setOpaque(false);
		txtSuma.setEditable(false);
		txtSuma.setColumns(10);
		txtSuma.setBounds(61, 236, 54, 26);
		panel.add(txtSuma);
		
		JCheckBox chckbxDescuento = new JCheckBox("Descuento");
		chckbxDescuento.setBounds(196, 289, 105, 29);
		panel.add(chckbxDescuento);
		
		txttieneDescuento = new JTextField();
		txttieneDescuento.setText("\u00BFTiene descuento?");
		txttieneDescuento.setOpaque(false);
		txttieneDescuento.setEditable(false);
		txttieneDescuento.setColumns(10);
		txttieneDescuento.setBounds(196, 236, 166, 26);
		panel.add(txttieneDescuento);
		
		txtdeQueTipo = new JTextField();
		txtdeQueTipo.setText("\u00BFDe que tipo?");
		txtdeQueTipo.setOpaque(false);
		txtdeQueTipo.setEditable(false);
		txtdeQueTipo.setColumns(10);
		txtdeQueTipo.setBounds(200, 330, 114, 26);
		panel.add(txtdeQueTipo);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Familiar", "Amigo", "Conocido", "Cliente habitual"}));
		comboBox_1.setBounds(200, 373, 114, 26);
		panel.add(comboBox_1);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBackground(SystemColor.desktop);
		progressBar.setForeground(new Color(0, 204, 51));
		progressBar.setStringPainted(true);
		progressBar.setValue(75);
		progressBar.setBounds(785, 415, 249, 39);
		panel.add(progressBar);
		
		txtProcesandoCuenta = new JTextField();
		txtProcesandoCuenta.setText("Procesando cuenta");
		txtProcesandoCuenta.setOpaque(false);
		txtProcesandoCuenta.setEditable(false);
		txtProcesandoCuenta.setColumns(10);
		txtProcesandoCuenta.setBounds(832, 348, 156, 26);
		panel.add(txtProcesandoCuenta);
		
		txtvaAPagar = new JTextField();
		txtvaAPagar.setText("\u00BFVa a pagar con efectivo o tarjeta?");
		txtvaAPagar.setOpaque(false);
		txtvaAPagar.setEditable(false);
		txtvaAPagar.setColumns(10);
		txtvaAPagar.setBounds(421, 290, 263, 26);
		panel.add(txtvaAPagar);
		
		JRadioButton rdbtnEfectivo = new JRadioButton("Efectivo");
		buttonGroup_1.add(rdbtnEfectivo);
		rdbtnEfectivo.setBounds(421, 347, 87, 29);
		panel.add(rdbtnEfectivo);
		
		JRadioButton rdbtnTarjeta = new JRadioButton("Tarjeta");
		buttonGroup_1.add(rdbtnTarjeta);
		rdbtnTarjeta.setBounds(421, 394, 87, 29);
		panel.add(rdbtnTarjeta);
	}
}
